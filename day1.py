from functools import reduce

with open (r'C:\Users\jakub\Desktop\aoc\1.txt') as f:
    # potrebuju i > i - 1
    lines = [int(l.strip()) for l in f.readlines()]
    sum1 = sum([1 for e in range(1, len(lines)) if lines[e] > lines[e-1]])
    sum2 = sum([1 for e in range(4, len(lines)) if lines[e] > lines[e-3]])

with open('C:\\Users\\Dalibor\\Desktop\\advent of code 2021\\2.txt') as f:

    lines = [l.strip() for l in f.readlines()]

    # horizontal, # depth, # aim
    x, y, z = 0, 0, 0

    def conds(e):
        global x, y, z
        num = int(e[-1])
        if 'up' in e: z -= num
        if 'down' in e: z += num
        if 'forward' in e:
            x += num
            y += num * z
        print(x, y, z)

    filter(conds, lines)
    print(x*y)


